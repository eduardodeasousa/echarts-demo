import getRandomInt from './generateRandomInt'

const generateSeries = () => {
  return {
    series: [
      {
        name: 'Ligações para celular',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'Ligações para longa distância',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'Ligações para celular',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'Bonus de minutos grátis',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'Franquia proporcional de minutos',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'Favoritos celular',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'Números (?)',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      },
      {
        name: 'DDI',
        type: 'bar',
        barWidth: 10,
        barGap: 1,
        emphasis: {
          focus: 'series'
        },
        data: [getRandomInt(0, 10), getRandomInt(0, 10), getRandomInt(0, 10)]
      }
    ]
  }
}

export default generateSeries
