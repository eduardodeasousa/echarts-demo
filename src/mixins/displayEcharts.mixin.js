const displayEcharts = {
  data () {
    return {
      isMobile: false
    }
  },
  created () {
    this.onResize()
  },
  mounted () {
    this.$nextTick(() => {
      window.addEventListener('resize', this.onResize)
    })
  },
  methods: {
    onResize () {
      const width = window.innerWidth
      this.isMobile = width < 1200
    }
  }
}

export default displayEcharts
